<?php


namespace Vxsoft\Exam\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vxsoft\Exam\Entity\Exam;
use Vxsoft\Exam\Form\ExamType;
use Vxsoft\Main\Service\MainService;

/**
 * Class ExamController
 * @package Vxsoft\Exam\Controller
 * @Route("/admin/exam")
 */
class ExamController extends AbstractController
{
    private $em;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("/", name="vx_admin_exam_list")
     * @param Request $request
     */
    public function listAction(Request $request){

        $exams = $this->em->getRepository(Exam::class)->findBy(['deleted'=> 0]);
        return $this->render('Exam/list.html.twig', compact('exams'));
    }

    /**
     * @Route("/create", name="fn_admin_exam_create")
     * @Route("/{exam}/update", name="vx_admin_exam_update")
     * @param Request $request
     * @param Exam $exam
     * @return Response
     */
    public function createAction(Request $request, Exam $exam): Response
    {
        $form = $this->createForm(ExamType::class, $exam);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            try {
                $exam = $form->getData();
                $exam->setCreatedBy($this->getUser());
                $exam->setCreatedAt(new \DateTime());
                $exam->setUpdatedAt(new \DateTime());

                $this->em->persist($exam);
                $this->em->flush();
                $this->addFlash('success', 'Successfully Added Exam');
                return $this->redirectToRoute('vx_admin_exam_list');

            }catch (\Exception $exception)
            {
                dd($exception);
            }
        }

        $form = $form->createView();
        return $this->render('Exam/create.html.twig', compact('form'));
    }


    /**
     * @param Request $request
     * @param Exam $exam
     * @param MainService $mainService
     * @return RedirectResponse
     * @Route("/{exam}/delete", name="vx_exam_delete")
     */
    public function deleteFaculty(Request $request, Exam $exam, MainService $mainService): RedirectResponse
    {
        $mainService->softDelete($exam);
        $this->addFlash('success', 'Exam Deleted');
        return $this->redirectToRoute('vx_admin_exam_list');
    }
}